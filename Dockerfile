FROM alpine

WORKDIR /app

RUN apk add --update --no-cache \
        docker \
        docker-compose \
        git

ENTRYPOINT ["sh"]

CMD ["/app/run.sh"]
